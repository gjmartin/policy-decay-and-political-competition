using Gadfly
import Cairo, Fontconfig

function contour_plot(value_df, out_dir)
    contour = plot(sort(value_df, [:Player, :Proposer]),
        ygroup = "Player",
        xgroup = "Proposer",
        x = "x",
        y = "q",
        z = "value",
        Geom.subplot_grid(Geom.contour), 
        Scale.color_continuous(minvalue=-5.0, maxvalue=0.0))

    push!(contour, 
        Guide.colorkey(title = "Value"))
    push!(contour,
        Theme(panel_fill = colorant"white",
        background_color = colorant"white",
            ))
        
    contour |> PNG("$out_dir/value_contours.png", 16cm, 16cm)
end

function line_plot(value_df, out_dir)
    line = plot(@subset(value_df, (:Player .== "R") .& (:q .== 0.0)),
        group = "Proposer",
        linestyle = "Proposer",
        color = "Proposer",
        x = "x",
        y = "value",
        Geom.line,
        Scale.color_discrete_manual("gray20", "gray80", levels = ["L", "R"]),
        Scale.linestyle_discrete(levels = ["L", "R"])
    )

    push!(line,
        Theme(panel_fill = colorant"white",
        background_color = colorant"white",
        line_width=1mm
            ))
    push!(line, Guide.ylabel("V_R(x, 0, P)"))


    line |> PNG("$out_dir/proposal_power.png", 16cm, 16cm)
end

function dot_plot(prop_df, out_dir, π_R)
    dot = plot(prop_df,
        color = "Region",
        x = "xbin",
        y = "qbin",
        Geom.rectbin,
        Coord.Cartesian(ymin=-4.0,ymax=0.0,xmin = 0.0, xmax = eval(π_R)),
        Scale.color_discrete_manual("gray20", "gray50", "gray80")
    )

    push!(dot,
        Theme(panel_fill = colorant"white",
        background_color = colorant"white",
        ))

    push!(dot, Guide.xlabel("x"))
    push!(dot, Guide.ylabel("q"))

    dot |> PNG("$out_dir/proposal_regions.png", 16cm, 16cm)
end

function vec_plot(value_df, out_dir, π_R, x_step, q_step)
    
    plot_points = @chain value_df begin
        @subset(:Player .== "R",
                :Proposer .== "R",
                :q .>= -4,
                abs.(rem.(:x ./ x_step, 10))  .< 1e-8,
                abs.(rem.(:q  ./ q_step, 25))  .< 1e-8)
        @transform(:Region = :proposal_region)
    end

    vecfield = plot(plot_points,
	x = "x", xend = "proposal",
	y = "q", yend = "q",
	color = "Region",
	Geom.segment(arrow = true, filled = true),
	Coord.Cartesian(ymin=-4.0,ymax=0.0,xmin = 0.0, xmax = eval(π_R)),
	Scale.x_continuous(minvalue=0.0, maxvalue=1.0),
	Scale.y_continuous(minvalue=-4.0, maxvalue=0.0),
	Scale.color_discrete_manual("gray20", "gray50", "gray80"))

    push!(vecfield,
        Theme(panel_fill = colorant"white",
        background_color = colorant"white",
        ))
    vecfield |> PNG("$out_dir/proposal_vectors.png")
end

function prob_plot(decay_likelihood, out_dir, π_R)
    probplot = plot(decay_likelihood,
	x = "x",
	color = "Proposer",
	linestyle = "Proposer",
	y = "Probability",
	Geom.line,
	Coord.Cartesian(ymin=0.0,ymax=1.0,xmin = 0.0, xmax = eval(π_R)),
	Scale.color_discrete_manual("gray20", "gray80"))

    push!(probplot,
        Theme(panel_fill = colorant"white",
        background_color = colorant"white",
        line_width=0.75mm
        ))
    push!(probplot, Guide.ylabel("Decay Probability"))

    probplot |> PNG("$out_dir/decay_likelihood.png")

end

function spell_plot(spells, out_dir, x_max)
    spells.opacity = 0.2 .+ 0.8 .* (spells.nspells ./ maximum(spells.nspells))

    med = layer(@subset(spells, :Distance .<= x_max),
		x = "Distance",
		y = "length_p50",
		alpha = "opacity",
		Geom.point)

	spellplot = plot(med,
		Coord.Cartesian(ymin=0.0, ymax=40.0))

	push!(spellplot,
		Theme(panel_fill = colorant"white",
		background_color = colorant"white",
		line_width = 1mm
		))
	push!(spellplot, Guide.ylabel("Median Duration (periods)"))
	push!(spellplot, Guide.xlabel("Distance from Ideal Point"))


	spellplot |> PNG("$out_dir/expected_duration.png")
end

function bin2d_plot(pathdata, out_dir)
	bin2d = plot(pathdata,
		x = "x", 
		y = "q",
		xgroup = "Proposer",
		Geom.subplot_grid(
			Geom.histogram2d(xbincount=50, ybincount = 50))
		)

	push!(bin2d,
		Theme(panel_fill = colorant"white",
		background_color = colorant"white",
		))

	bin2d |> PNG("$out_dir/simulated_bin2d.png")
end

function x_dens_plot(pathdata, out_dir, π_R)
    x_dens = plot(pathdata,
    x = "x", 
    color = "Proposer",
    Geom.density(bandwidth = 0.05),
    Coord.Cartesian(xmin = 0.0, xmax = eval(π_R)),
    Scale.color_discrete_manual("gray20", "gray80")
    )

    push!(x_dens,
        Theme(panel_fill = colorant"white",
        background_color = colorant"white",
        line_width = 1mm
        ))

    x_dens |> PNG("$out_dir/simulated_x_density.png")
end