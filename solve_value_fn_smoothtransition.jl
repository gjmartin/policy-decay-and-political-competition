using Statistics
using Distributions
using DataFrames
using DataFramesMeta
using StatsBase
using ArgParse
import Random
import CSV

include("plotting.jl")

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table s begin
        "--pi"
            help = "R's ideal point. Must be positive."
			arg_type = Float64
			default = 1.0
			range_tester = >(0)
        "--aR"
            help = "R's utility weight on policy. Must be positive."
            arg_type = Float64
            default = 1.0
			range_tester = >(0)
		"--aL"
            help = "L's utility weight on policy. Must be positive."
            arg_type = Float64
            default = 1.0
			range_tester = >(0)
		"--delta"
            help = "Discount rate, in (0,1)."
            arg_type = Float64
            default = 0.9
			range_tester = x -> ((x > 0) & (x < 1))
        "--lambda"
            help = "Exponential rate parameter for decay distribution. Must be positive."
            arg_type = Float64
            default = 1.0
			range_tester = >(0)
		"--tol"
			help = "Tolerance for value function convergence. Must be positive, less than 0.1"
			arg_type = Float64
			default = 1e-3
			range_tester = x -> ((x > 0) & (x <= 1e-2))
		"--maxiter"
			help = "Maximum number of value function iterations. Set to 0 to skip solve and run from stored values (assumes existence of a file called solved_values.csv in the specified output directory)."
			arg_type = Int
			default = 100
			range_tester = >=(0)
		"--xstep"
			help = "Grid step size, x dimension. Allowable values between 1e-6 and 1e-1."
			arg_type = Float64
			default = 0.005
			range_tester = x -> ((x > 1e-6) & (x <= 1e-1))
		"--qstep"
			help = "Grid step size, q dimension. Allowable values between 1e-6 and 1e-1."
			arg_type = Float64
			default = 0.01
			range_tester = x -> ((x > 1e-6) & (x <= 1e-1))
		"--seed"
			help = "Random seed for simulated paths"
			arg_type = Int
			default = 9721475
			range_tester = >(0)
		"--nsim"
			help = "Number of simulated paths to generate. Set to 0 to skip sim step."
			arg_type = Int
			default = 1000
			range_tester = >=(0)
		"--simT"
			help = "Number of periods per simulated path. Ignored if nsim == 0."
			arg_type = Int
			default = 1000
			range_tester = >(0)
        "--transitionSlope"
            help = "Slope of power transition function (with respect to decay). Must be positive."
            arg_type = Float64
            default = 1.0
            range_tester = >(0)
        "output_dir"
            help = "Directory location for output to go."
            required = true
		"output_subdir"
			help = "Subfolder for output from this run."
			required = false
    end

    return parse_args(s)
end


### parse arguments from commandline ###
parsed_args = parse_commandline()

out_dir = (parsed_args["output_subdir"] == nothing) ? parsed_args["output_dir"] : joinpath(parsed_args["output_dir"], parsed_args["output_subdir"])
if !ispath(out_dir)
	mkdir(out_dir)
end

println("Writing output to $out_dir")

println("Arguments:")
for (arg,val) in parsed_args
	if !occursin(r"_dir", arg)
    	println("  $arg  =>  $val")
	end
end

### globals ###

# model parameters
π_R = parsed_args["pi"]     # R's ideal point in x dimension
α_L = parsed_args["aL"]     # L utility weight on x dimension
α_R = parsed_args["aR"]		# R's utility weight on x dimension 
δ   = parsed_args["delta"]  # (common) discount rate
r_λ = parsed_args["lambda"] # exponential parameter for decay distribution	
β = parsed_args["transitionSlope"] # slope of power transition function

# optimization controls
tol 	 = 	parsed_args["tol"]
iter_max = 	parsed_args["maxiter"]
x_step   = 	parsed_args["xstep"]
q_step   = 	parsed_args["qstep"]

# set random seed for simulations
rng = Random.MersenneTwister(parsed_args["seed"])

# grid definition
# define with columns as slices at constant q (for efficiency)
q_min = δ / (1 - δ) * π_R^2 * max(α_R, α_L)

n_x = convert(Int64, floor(π_R / x_step) + 1)
n_q = convert(Int64, floor(q_min / q_step) + 1)
grid_q = -(0:q_step:q_min)
grid_x = (0:x_step:π_R)

# value functions
v_R_R = zeros(Float64, n_x, n_q)
v_R_L = zeros(Float64, n_x, n_q)
v_L_R = zeros(Float64, n_x, n_q)
v_L_L = zeros(Float64, n_x, n_q)


### distribution for lambda draws: standard exponential ###
dexp = x -> pdf(Exponential(r_λ), x)
pexp = x -> cdf(Exponential(r_λ), x)
rexp = n -> Random.randexp(rng, n) * r_λ


## evaluate probability for every lambda on the grid
f_lambda = diff([0; pexp.(0:q_step:q_min)])

### utility functions ###
function u_L(x,q; a = 1.0)
	-a * (x^2) + q
end

function u_R(x,q; a = 1.0, p = 1.0)
    -a * (p-x)^2 + q
end

## eval and store for every point on grid
u_R_xq = u_R.(collect(grid_x), reshape(collect(grid_q), 1, n_q); a = α_R, p = π_R)
u_L_xq = u_L.(collect(grid_x), reshape(collect(grid_q), 1, n_q); a = α_L)


## power transition function
function power_transition(q, β)
    1 - exp(β * grid_q[q])
end

### initialize value functions: 
### assume return to frontier at Voter's myopic indifference point, then stay there forever
for q in 1:n_q
	for x in 1:n_x
		v_R_R[x,q] = 1 / (1 - δ) * u_R_xq[findlast(u_L_xq[:,1] .>= u_L_xq[x,q]),1]
		v_R_L[x,q] = 1 / (1 - δ) * u_R_xq[findfirst(u_R_xq[:,1] .>= u_R_xq[x,q]),1]
		v_L_R[x,q] = 1 / (1 - δ) * u_L_xq[findlast(u_L_xq[:,1] .>= u_L_xq[x,q]),1]
		v_L_L[x,q] = 1 / (1 - δ) * u_L_xq[findfirst(u_R_xq[:,1] .>= u_R_xq[x,q]),1]
    end
end


### equilibrium correspondences ###
## L proposing. assumes proposal on frontier
function proposal_L(x0::Int,
					q0::Int,
					a_L::BitVector,
					u_R_xq::Array{Float64,2},
					u_L_xq::Array{Float64,2},
					v_R_L::Array{Float64,2},
					v_L_L::Array{Float64,2},
					v_R_R::Array{Float64,2},
					v_L_R::Array{Float64,2}; 
					delta = 0.9)
    
    # probability of transition if decay persists
    p = power_transition(q0, β)

	# find all points on frontier acceptable to L 
	@views a_L .= ((u_L_xq[:,1] .- u_L_xq[x0,q0] .+ delta .* (v_L_L[:,1] .- p * v_L_R[x0,q0] .- (1 - p) * v_L_L[x0,q0])) .>= 0)
	
	# AND acceptable to R
	@views a_L .&= ((u_R_xq[:,1] .- u_R_xq[x0,q0] .+ delta .* (v_R_L[:,1] .- p * v_R_R[x0,q0] .- (1 - p) * v_R_L[x0,q0])) .>= 0)

	x_star = findfirst(a_L)
	
	# if no such proposal exists
	if isnothing(x_star)
		# SQ realized, R takes power probabilistically
		return (x = x0,
				q = q0,
				pr = 1 - p)
	else
		# return the closest to L's ideal, L retains power
		return (x = x_star :: Int64,
				q = 1,
				pr = 1.0)
	end

	
end

## R proposing, assumes proposal on frontier
function proposal_R(x0::Int,
	q0::Int,
	a_R::BitVector,
	u_R_xq::Array{Float64,2},
	u_L_xq::Array{Float64,2},
	v_R_L::Array{Float64,2},
	v_L_L::Array{Float64,2},
	v_R_R::Array{Float64,2},
	v_L_R::Array{Float64,2}; 
	delta = 0.9)

    # probability of transition if decay persists
    p = power_transition(q0, β)

	# find all points on frontier acceptable to R 
	@views a_R .= ((u_R_xq[:,1] .- u_R_xq[x0,q0] .+ delta .* (v_R_R[:,1] .- p * v_R_L[x0,q0] .- (1 - p) * v_R_R[x0,q0])) .>= 0)

	# AND acceptable to L
	@views a_R .&= ((u_L_xq[:,1] .- u_L_xq[x0,q0] .+ delta .* (v_L_R[:,1] .- p * v_L_L[x0,q0] .- (1 - p) * v_L_R[x0,q0])) .>= 0)

	x_star = findlast(a_R)
	
	# if no such proposal exists
	if isnothing(x_star)
		# SQ realized, L takes power probabilistically
		return (x = x0,
				q = q0,
				pr = p)
	else
		# return the closest to R's ideal, R retains power
		return (x = x_star :: Int64,
				q = 1,
				pr = 0.0)
	end
end

## convenience fn to eval on data frame
## expects value fns exist
function eqm_proposal_full(
	x0,
	q0,
	prob_L_proposing)

	x1_L, q1_L, p_L = proposal_L(findfirst(grid_x .== x0),
				   findfirst(grid_q .== q0),
				   accept_vec,
				   u_R_xq, u_L_xq,
				   v_R_L, v_L_L, v_R_R, v_L_R, delta = δ)

	x1_R, q1_R, p_R = proposal_R(findfirst(grid_x .== x0),
					findfirst(grid_q .== q0),
					accept_vec,
					u_R_xq, u_L_xq,
					v_R_L, v_L_L, v_R_R, v_L_R, delta = δ)

	(prob_L_proposing * grid_x[x1_L] + (1 - prob_L_proposing) * grid_x[x1_R],
	 prob_L_proposing * grid_q[q1_L] + (1 - prob_L_proposing) * grid_q[q1_R],
	 prob_L_proposing * p_L + (1 - prob_L_proposing) * p_R)
end

function eqm_proposal(
	x0,
	q0,
	prob_L_proposing)

	x1_L, q1_L, p_L = proposal_L(findfirst(grid_x .== x0),
				   findfirst(grid_q .== q0),
				   accept_vec,
				   u_R_xq, u_L_xq,
				   v_R_L, v_L_L, v_R_R, v_L_R, delta = δ)

	x1_R, q1_R, p_R = proposal_R(findfirst(grid_x .== x0),
					findfirst(grid_q .== q0),
					accept_vec,
					u_R_xq, u_L_xq,
					v_R_L, v_L_L, v_R_R, v_L_R, delta = δ)

	prob_L_proposing * grid_x[x1_L] + (1 - prob_L_proposing) * grid_x[x1_R]
end

function eqm_decay(
	x0,
	q0,
	prob_L_proposing)

	x1_L, q1_L, p_L = proposal_L(findfirst(grid_x .== x0),
	findfirst(grid_q .== q0),
	accept_vec,
	u_R_xq, u_L_xq,
	v_R_L, v_L_L, v_R_R, v_L_R, delta = δ)

	x1_R, q1_R, p_R = proposal_R(findfirst(grid_x .== x0),
	 findfirst(grid_q .== q0),
	 accept_vec,
	 u_R_xq, u_L_xq,
	 v_R_L, v_L_L, v_R_R, v_L_R, delta = δ)

	prob_L_proposing * (1 - p_L) + (1 - prob_L_proposing) * p_R

end


### value function update step ###

## update step, L proposing
function integrate_proposal_L!(
	x0::Int64,
	q0::Int64, 
	f_lambda::Vector{Float64}, 
	accept_vec::BitVector, 
	u_R_xq::Array{Float64,2},
	u_L_xq::Array{Float64,2},
	v_R_L::Array{Float64,2},
	v_L_L::Array{Float64,2},
	v_R_R::Array{Float64,2},
	v_L_R::Array{Float64,2}, 
	n_x::Int64,
	n_q::Int64; 
	delta=0.9)
	L = 0.0
	R = 0.0
	for q in q0:n_q 
		@views f_shifted = q == n_q ? 1 - sum(f_lambda[1:(n_q-q0)]) : f_lambda[q - q0 + 1]
		p_x, p_q, p_p = proposal_L(x0,q,accept_vec,u_R_xq,u_L_xq,v_R_L,v_L_L,v_R_R,v_L_R; delta=delta)
		L += f_shifted * (u_L_xq[p_x, p_q] + delta * (p_p * v_L_L[p_x, p_q] + (1 - p_p) * v_L_R[p_x, p_q]))
		R += f_shifted * (u_R_xq[p_x, p_q] + delta * (p_p * v_R_L[p_x, p_q] + (1 - p_p) * v_R_R[p_x, p_q]))
	end
	v_L_L[x0,q0]=L
	v_R_L[x0,q0]=R
end


## update step, R proposing
function integrate_proposal_R!(
	x0::Int64,
	q0::Int64, 
	f_lambda::Vector{Float64}, 
	accept_vec::BitVector, 
	u_R_xq::Array{Float64,2},
	u_L_xq::Array{Float64,2},
	v_R_L::Array{Float64,2},
	v_L_L::Array{Float64,2},
	v_R_R::Array{Float64,2},
	v_L_R::Array{Float64,2}, 
	n_x::Int64,
	n_q::Int64; 
	delta=0.9)
	L = 0.0
	R = 0.0

	for q in q0:n_q
		@views f_shifted = q == n_q ? 1 - sum(f_lambda[1:(n_q-q0)]) : f_lambda[q - q0 + 1]
		p_x,p_q,p_p = proposal_R(x0,q,accept_vec,u_R_xq,u_L_xq,v_R_L,v_L_L,v_R_R,v_L_R; delta=delta)
		L += f_shifted * (u_L_xq[p_x, p_q] + delta * (p_p * v_L_L[p_x, p_q] + (1 - p_p) * v_L_R[p_x, p_q]))
		R += f_shifted * (u_R_xq[p_x, p_q] + delta * (p_p * v_R_L[p_x, p_q] + (1 - p_p) * v_R_R[p_x, p_q]))
	end
	v_L_R[x0,q0]=L
	v_R_R[x0,q0]=R
end


### main loop ###
max_chg = 1.0
iter_ct = 1

accept_vec = falses(n_x)

while ((max_chg > tol) & (iter_ct <= iter_max)) 
    
    println("iteration $iter_ct")

	global max_chg = 0
	@time for q in 1:n_q
		if ((q % (n_q ÷ 10)) == 0)
			print(".")
		end
		for x in 1:n_x 
			ll0 = v_L_L[x,q]
			lr0 = v_L_R[x,q]
			rl0 = v_R_L[x,q]
			rr0 = v_R_R[x,q]

			integrate_proposal_L!(x, q, f_lambda, accept_vec, u_R_xq,u_L_xq,v_R_L,v_L_L,v_R_R,v_L_R, n_x, n_q; delta=δ)
			integrate_proposal_R!(x, q, f_lambda, accept_vec, u_R_xq,u_L_xq,v_R_L,v_L_L,v_R_R,v_L_R, n_x, n_q; delta=δ)

			global max_chg = max(max_chg,
						  abs(ll0 - v_L_L[x,q]),
						  abs(lr0 - v_L_R[x,q]),
						  abs(rl0 - v_R_L[x,q]),
						  abs(rr0 - v_R_R[x,q]))
        end
    end
	println("max diff = $max_chg")
	global iter_ct += 1
end


function stack_and_label(v, player, proposer)
	vdf = DataFrame(v, :auto) |> stack
	vdf.q = repeat(grid_q, inner = length(grid_x))
	vdf.x = repeat(grid_x, outer = length(grid_q))
	vdf.Player .= player
	vdf.Proposer .= proposer
	select(vdf, Not(:variable))
end

#### value function solve done ####

## save / load ##
value_df = vcat(
		stack_and_label(v_R_R, "R", "R"),
		stack_and_label(v_R_L, "R", "L"),
		stack_and_label(v_L_L, "L", "L"),
		stack_and_label(v_L_R, "L", "R")
	)

if iter_max > 0
	# store result of this run
	value_df |> CSV.write("$out_dir/solved_values.csv")
end

# otherwise, use saved version
value_df = CSV.File("$out_dir/solved_values.csv") |> DataFrame

## populate v_* arrays if we haven't just solved the value function
if iter_max == 0
	v_R_R .= reshape(@subset(value_df, :Proposer .== "R", :Player .== "R").value, length(grid_x), length(grid_q))
	v_R_L .= reshape(@subset(value_df, :Proposer .== "L", :Player .== "R").value, length(grid_x), length(grid_q))
	v_L_R .= reshape(@subset(value_df, :Proposer .== "R", :Player .== "L").value, length(grid_x), length(grid_q))
	v_L_L .= reshape(@subset(value_df, :Proposer .== "L", :Player .== "L").value, length(grid_x), length(grid_q))
end

### plot value functions 

## contour
contour_plot(value_df, out_dir)

## line - slice at frontier
line_plot(value_df, out_dir)

### compute equilibrium proposals (by R) for every point on grid given converged value functions
transform!(value_df,
	:Proposer => (x -> ifelse.(x .== "R", 0.0, 1.0)) => :prob_L_proposing)

transform!(value_df,
	[:x, :q, :prob_L_proposing] => ByRow(eqm_proposal) => :proposal)
transform!(value_df,
	[:x, :q, :prob_L_proposing] => ByRow(eqm_decay) => :decay)

value_df.proposal_region = 
	[value_df.decay[i] > 0 ? "Decay on path" : (value_df.proposal[i] >= value_df.x[i] ? "Receiver concedes" : "Proposer concedes") for i in 1:nrow(value_df)]

prop_df = @chain value_df begin
	@subset(:Player .== "R", :Proposer .== "R", :q .>= -5)
	@transform(:xbin = round.(:x, digits = 2),
			   :qbin = round.(:q, digits = 2))
	groupby([:xbin, :qbin])
	combine(:proposal_region => mode => :Region)
	sort([:Region, :xbin, :qbin])
end

# Plot equilibrium regions
dot_plot(prop_df, out_dir, π_R)

#Plot the gradient field 
vec_plot(value_df, out_dir, π_R, x_step, q_step)


decay_likelihood = @chain value_df begin
	@subset(:Player .== "L")
	groupby([:x, :Proposer])
	combine(:proposal_region => (x -> sum((x .== "Decay on path") .* f_lambda)) => :Probability)
end


prob_plot(decay_likelihood, out_dir, π_R)


### simulation, if requested
nT = parsed_args["simT"]
nsim = parsed_args["nsim"]

path_x = zeros(Float64, nT, nsim)
path_q = zeros(Float64, nT, nsim)
path_p = zeros(Bool, nT, nsim)

p0s = rand(rng, Bool, nsim)
x0s = rand(rng, nsim)
lambdas = reshape(rexp(nT*nsim), (nT, nsim))

function snap_to(x, grid)
	ind = findfirst(abs(x) .<= abs.(grid))
	if isnothing(ind)
		grid[length(grid)]
	else
		grid[ind]
	end
end

function sim_path!(i, x0, nT)
	lambda = lambdas[:,i]
	x, q, p = eqm_proposal_full(snap_to(x0, grid_x), snap_to(-lambda[1], grid_q), p0s[i] == 0 ? "L" : "R")
	path_x[1, i] = x
	path_q[1, i] = q
	path_p[1, i] = p
	for t in 2:nT
		x, q, p = eqm_proposal_full(snap_to(path_x[t-1, i], grid_x), snap_to(path_q[t-1, i]-lambda[t], grid_q), path_p[t-1,i] == 0 ? "L" : "R")
		path_x[t, i] = x
		path_q[t, i] = q
		path_p[t, i] = p
	end
end

if nsim > 0
	for sim in 1:nsim
		sim_path!(sim, x0s[sim], nT)
	end

	pathdata = DataFrame(
		x = vec(path_x),
		q = vec(path_q),
		Proposer = [pr == 0 ? "L" : "R" for pr in vec(path_p)],
		sim = repeat(1:nsim, inner = nT))

	pathdata |> CSV.write("$out_dir/simulated_paths.csv")

	welfare = combine(pathdata, 
					  [:x, :q] => ((x, q) -> mean(u_L.(x, q, a = α_L))) => :L,
				 	  [:x, :q] => ((x, q) -> mean(u_R.(x, q, a = α_R, p = π_R))) => :R)

	welfare |> CSV.write("$out_dir/welfare.csv")

	spells = @chain pathdata begin
		groupby(:sim)
		transform(:Proposer => (x -> cumsum([i == 1 ? true : p != x[i-1] for (i, p) in enumerate(x)])) => :spell)
		groupby([:sim, :spell])
		transform([:x, :Proposer] => ((x,p) -> p[1] == "L" ? x[1] : 1.0 - x[1]) => :Distance)
		groupby(:sim)
		@subset(:spell .!= maximum(:spell))
		groupby([:sim, :spell, :Distance])
		combine(:Proposer => length => :spell_length)
		groupby(:Distance)
		combine(:spell_length => median => :length_p50,
				:spell_length => (x -> quantile(x, 0.05)) => :length_p05,
				:spell_length => (x -> quantile(x, 0.95)) => :length_p95,
				:spell_length => length => :nspells)
	end

	x_max = maximum(@subset(value_df, :proposal_region .== "Decay on path", :Proposer .== "R").x)

	spell_plot(spells, out_dir, x_max)

	sort!(pathdata,[:Proposer])

	bin2d_plot(pathdata, out_dir)

	x_dens_plot(pathdata, out_dir, π_R)
	
end



