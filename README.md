# Policy Decay and Political Competition

This archive contains scripts used to generate numerical and simulation results in Callander and Martin (2024), "Policy Decay and Political Competition." 

## Initial Setup

You will need [Julia](https://julialang.org/downloads/) version 1.9.2 or higher installed on your system. Once installed, you will also need the following packages, which can be installed from the Julia prompt with: 

```
using Pkg
Pkg.add(["Distributions", "DataFrames", "DataFramesMeta", "Gadfly", "StatsBase", "ArgParse", "Cairo", "Fontconfig", "CSV"])
```

## Replication

To produce all figures used in the paper, run the script `comparative_statics.sh` from the same directory where you cloned the archive:

```
sh comparative_statics.sh .
```

The `.` after the script indicates that output will be placed in subfolders beneath the directory containing the archive. You can change this to direct output to a different directory.

## Running the solver with other parameters

The main script that solves the value functions and produces plots is `solve_value_fn.jl`. There are two variants, `solve_value_fn_uniform.jl` and `solve_value_fn_lognormal.jl`, which change the distribution of decay draws but are otherwise identical, and an additional variant `solve_value_fn_smoothtransition.jl` which changes the transition rule to use a smooth function (the exponential CDF) of realized decay to determine the transition probability. These scripts have a number of configurable options which you can see by running `julia solve_value_fn.jl --help`:

```
julia solve_value_fn.jl --help
usage: solve_value_fn.jl [--pi PI] [--aR AR] [--aL AL] [--delta DELTA]
                        [--lambda LAMBDA] [--tol TOL]
                        [--maxiter MAXITER] [--xstep XSTEP]
                        [--qstep QSTEP] [--seed SEED] [--nsim NSIM]
                        [--simT SIMT] [-h] output_dir [output_subdir]

positional arguments:
  output_dir         Directory location for output to go.
  output_subdir      Subfolder for output from this run.

optional arguments:
  --pi PI            R's ideal point. Must be positive. (type:
                     Float64, default: 1.0)
  --aR AR            R's utility weight on policy. Must be positive.
                     (type: Float64, default: 1.0)
  --aL AL            L's utility weight on policy. Must be positive.
                     (type: Float64, default: 1.0)
  --delta DELTA      Discount rate, in (0,1). (type: Float64, default:
                     0.9)
  --lambda LAMBDA    Exponential rate parameter for decay
                     distribution. Must be positive. (type: Float64,
                     default: 1.0)
  --tol TOL          Tolerance for value function convergence. Must be
                     positive, less than 0.1 (type: Float64, default:
                     0.001)
  --maxiter MAXITER  Maximum number of value function iterations. Set
                     to 0 to skip solve and run from stored values
                     (assumes existence of a file called
                     solved_values.csv in the specified output
                     directory). (type: Int64, default: 100)
  --xstep XSTEP      Grid step size, x dimension. Allowable values
                     between 1e-6 and 1e-1. (type: Float64, default:
                     0.005)
  --qstep QSTEP      Grid step size, q dimension. Allowable values
                     between 1e-6 and 1e-1. (type: Float64, default:
                     0.01)
  --seed SEED        Random seed for simulated paths (type: Int64,
                     default: 9721475)
  --nsim NSIM        Number of simulated paths to generate. Set to 0
                     to skip sim step. (type: Int64, default: 1000)
  --simT SIMT        Number of periods per simulated path. Ignored if
                     nsim == 0. (type: Int64, default: 1000)
  -h, --help         show this help message and exit
```



