#!/bin/bash
BASEDIR=$1

# baseline
julia $BASEDIR/solve_value_fn.jl $BASEDIR baseline

# change mean of decay
julia $BASEDIR/solve_value_fn.jl --lambda 0.5 $BASEDIR r_050
julia $BASEDIR/solve_value_fn.jl --lambda 1.5 $BASEDIR r_150

# change polarization
julia $BASEDIR/solve_value_fn.jl --pi 0.75 $BASEDIR p_075
julia $BASEDIR/solve_value_fn.jl --pi 1.25 $BASEDIR p_125

# change decay distribution
julia $BASEDIR/solve_value_fn_lognormal.jl $BASEDIR lognormal
julia $BASEDIR/solve_value_fn_uniform.jl $BASEDIR uniform

# change transition rule. don't produce simulation paths for these
julia $BASEDIR/solve_value_fn_smoothtransition.jl --nsim 0 --transitionSlope 1.0 $BASEDIR smoothtransition_1
julia $BASEDIR/solve_value_fn_smoothtransition.jl --nsim 0 --transitionSlope 2.0 $BASEDIR smoothtransition_2
julia $BASEDIR/solve_value_fn_smoothtransition.jl --nsim 0 --transitionSlope 10.0 $BASEDIR smoothtransition_10